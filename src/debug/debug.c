/*
 * debug.c
 *
 *  Created on: Feb 5, 2020
 *      Author: Damon A. Shaw (admins@vt.edu)
 */

// Import the debug status codes
#include "debug.h"

// Import the UART driver definitions
#include <ti/drivers/UART.h>

// Import the GPIO driver definitions
#include <ti/drivers/GPIO.h>
#include <ti/devices/cc32xx/inc/hw_types.h>
#include <ti/devices/cc32xx/driverlib/interrupt.h>

/* Board Header file */
#include "ti_drivers_config.h"

static UART_Handle uart;
#define UART_BAUDRATE 9600

/**
 * Initializes the debugger resources.
 */
void initDebug() {
    // Create a UART parameters object and define some standard
    UART_Params params;

    UART_Params_init(&params);
    params.baudRate = UART_BAUDRATE;
    params.writeMode = UART_MODE_BLOCKING;
    params.writeDataMode = UART_DATA_BINARY;
    params.readDataMode = UART_DATA_BINARY;
    params.readEcho = UART_ECHO_OFF;
    params.writeTimeout = UART_WAIT_FOREVER;
    params.dataLength = UART_LEN_8;
    params.stopBits = UART_STOP_ONE;

    // Connect the UART module to the UART handle.
    uart = UART_open(CONFIG_UART_0, &params);

    // Make sure nothing went wrong when opening the UART.
    if(uart == NULL) {
        // UART open failed, system abort.
        dbgOutputLoc(DLOC_DEBUG_UART_OPEN_FAIL);
        systemExit();
    }

    // Initialize the debug GPIO pins.
    GPIO_setConfig(CONFIG_GPIO_DB_0, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);
    GPIO_setConfig(CONFIG_GPIO_DB_1, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);
    GPIO_setConfig(CONFIG_GPIO_DB_2, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);
    GPIO_setConfig(CONFIG_GPIO_DB_3, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);
    GPIO_setConfig(CONFIG_GPIO_DB_4, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);
    GPIO_setConfig(CONFIG_GPIO_DB_5, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);
    GPIO_setConfig(CONFIG_GPIO_DB_6, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);
    GPIO_setConfig(CONFIG_GPIO_DB_7, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);

    // For LED testing purposes
    /* Configure the LED pin */
    GPIO_setConfig(CONFIG_GPIO_LED_0, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);
    GPIO_setConfig(CONFIG_GPIO_LED_1, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);
    GPIO_setConfig(CONFIG_GPIO_LED_2, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);

    GPIO_write(CONFIG_GPIO_LED_0, 0);
    GPIO_write(CONFIG_GPIO_LED_1, 0);
    GPIO_write(CONFIG_GPIO_LED_2, 0);
}

/**
 * Writes a given character to UART.
 */
void dbgUARTVal(unsigned char outVal) {

    // Write the character to UART.
    int_fast32_t writeStatusCode = UART_write(uart, &outVal, 1);

    // Check if the writing to UART was unsuccessful. If it was, abort.
    if(writeStatusCode == UART_STATUS_ERROR){
        dbgOutputLoc(DLOC_DEBUG_UART_WRITE_FAIL);
        systemExit();
    }
    else
        dbgOutputLoc(DLOC_UART_SEND_SCS);

}

/**
 * Writes a message (stored as a character array) to UART.
 *
 * @param msg A pointer to the first element in the character array. Usually
 * this is just the character array variable.
 * @param sizeOfBuffer The total size of the buffer. This is equal to the size
 * of a character times the number of characters. Usually using `sizeof()` the
 * character array variable returns the correct value.
 *
 * @example
 * const char myMessage[] = "This is my awesome message!\0";
 * dbgUARTMsg(myMessage, sizeof(myMessage));
 */
void dbgUARTMsg(const char* msg, const unsigned int sizeOfBuffer) {
    UART_write(uart, msg, sizeOfBuffer);
}

/**
 * Writes an integer, negative or positive, to UART.
 *
 * @param value The integer value to write to UART.
 *
 * @example
 * int myInteger = -28375;
 * dbgUARTInt(myInteger);
 */
void dbgUARTInt(int value) {
    // Establish a character array to store the string version of the integer.
    char str[10];

    // Translate the integer into the character array.
    sprintf(str, "%d", value);

    /*
     * Starting at the beginning of the character array, print characters that
     * are not string terminator characters. If a string terminator character is
     * encountered, stop printing.
     */
    int idx = 0;
    while(str[idx] != '\0') {
        dbgUARTVal(str[idx]);
        idx++;
    }

    // Print a string terminator character to signify the end of the string.
    dbgUARTVal('\0');
}

/**
 * Sets the eight debug GPIO lines according to a given position code.
 * Positions codes must be integer values less than or equal to 127.
 * The most significant bit on the GPIO lines is reserved for indicating
 * a newly written position code.
 *
 * @param outLoc A value indicating a position in the code. This value
 * must always be less than or equal to 127.
 */
void dbgOutputLoc(unsigned int outLoc) {

    static unsigned int killCount = 0;
    killCount++;
    if (killCount == 1500 && 0) systemExit();

    /*
     * Initialize the latest status code variable. We use this to determine if the status
     * code to write to GPIO was the latest one written. If not, the "new" status code bit
     * is set HIGH.
     */
    static unsigned int lastStatusCode = 0xFF;

    /*
     * Verify that the provided status code is in the range 0..127.
     * If it isn't then the provided value constitutes an error.
     */
    if(outLoc > 127) {
        dbgOutputLoc(DLOC_DEBUG_BAD_STATUS_CODE);
        systemExit();
        return;
    }

    /*
     * Check if the new status code is different from the one previously
     * written to GPIO.
     */
    unsigned int isNewStatusCodeDifferent = 0;
    if(lastStatusCode != outLoc) {
        isNewStatusCodeDifferent = 1;
    }

    /*
     * Update the GPIO pins to reflect the new status code and the flag signifying if
     * the status code is different from the previously seen status code.
     */
    GPIO_write(CONFIG_GPIO_DB_0, ((outLoc >> 0) & 1)  );
    GPIO_write(CONFIG_GPIO_DB_1, ((outLoc >> 1) & 1)  );
    GPIO_write(CONFIG_GPIO_DB_2, ((outLoc >> 2) & 1)  );
    GPIO_write(CONFIG_GPIO_DB_3, ((outLoc >> 3) & 1) );
    GPIO_write(CONFIG_GPIO_DB_4, ((outLoc >> 4) & 1) );
    GPIO_write(CONFIG_GPIO_DB_5, ((outLoc >> 5) & 1) );
    GPIO_write(CONFIG_GPIO_DB_6, ((outLoc >> 6) & 1) );
    GPIO_write(CONFIG_GPIO_DB_7, isNewStatusCodeDifferent );

    /*
     * Update the last seen status code to the status code we're currently updating
     * from.
     */
    lastStatusCode = outLoc;
}

void debugToggleRedLED() {
    GPIO_toggle(CONFIG_GPIO_LED_0);
}

void debugToggleYellowLED() {
    GPIO_toggle(CONFIG_GPIO_LED_1);
}

void debugToggleGreenLED() {
    GPIO_toggle(CONFIG_GPIO_LED_2);
}


/**
 * Aborts all current operations on the system (except for itself) and
 * outputs the SYSTEM_ABORT status code to the debug lines. This will
 * also mask all interrupts, stopping them and whatever tasks that depend
 * on them.
 */
void systemExit() {

    // Stop interrupts by closing handles to interrupt driver peripherals
    //disableTimerOne();
    //disableTimerTwo();
    //UART_close(uart);

    // Print the SYSTEM_ABORT status code to reflect a system abort.
    dbgOutputLoc(DLOC_SYSTEM_ABORT);
    IntMasterDisable();
    while(1) {
        debugToggleGreenLED();
    }
}
