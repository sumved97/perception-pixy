/*
 * debug.h
 *
 *  Created on: Feb 5, 2020
 *      Author: Damon A. Shaw (admins@vt.edu)
 */

#ifndef DEBUG_H_
#define DEBUG_H_

#include <stdio.h>


// Debugging status codes.
#define DLOC_PIXY_COMM_TASK_FAIL        0x00
#define DLOC_TEST_THREAD_FAIL       0x01
#define DLOC_PIXY_THREAD_FAIL       0x02

#define DLOC_DEBUG_UART_OPEN_FAIL   0x10
#define DLOC_DEBUG_UART_WRITE_FAIL  0x11
#define DLOC_UART_SEND_SCS          0x12
#define DLOC_DEBUG_BAD_STATUS_CODE  0x13
#define DLOC_SYSTEM_ABORT           0x14

#define DLOC_SPI_INIT               0x20
#define DLOC_TIMER_INIT             0x21
#define DLOC_TIMER_OPEN_FAIL        0x22
#define DLOC_TIMER_START_FAIL       0x23
#define DLOC_SPI_OPEN_FAIL          0x24

#define DLOC_PIXY_QUEUE_INIT_FAIL   0x30
#define DLOC_PIXY_QUEUE_PUSH_FAIL   0x31
#define DLOC_PIXY_QUEUE_POP_FAIL    0x32
#define DLOC_TIMER_QUEUE_INIT_FAIL  0x33
#define DLOC_TIMER_QUEUE_PUSH_FAIL  0x34
#define DLOC_TIMER_QUEUE_POP_FAIL   0x35



// Debugging functions.
void initDebug();
void dbgUARTVal(unsigned char outVal);
void dbgUARTMsg(const char* msg, const unsigned int sizeOfBuffer);
void dbgUARTInt(int value);
void dbgOutputLoc(unsigned int outLoc);
void systemExit();

void debugToggleRedLED();
void debugToggleYellowLED();
void debugToggleGreenLED();

#endif /* DEBUG_H_ */
