/*
 * pixy_spi.c
 *
 *  Created on: Mar 3, 2020
 *      Author: Sumved Ravi
 */

// Standard library imports
#include <stddef.h>
#include <string.h>

// Driver configuration
#include <ti/drivers/Board.h>
#include <ti/drivers/Timer.h>
#include <ti/drivers/UART.h>
#include <ti/drivers/GPIO.h>
#include <ti/drivers/SPI.h>
#include "ti_drivers_config.h"

// Custom queues
#include "src/queues/timer_queue.h"
#include "src/queues/tx_queue.h"
#include "src/queues/rx_queue.h"

// Custom drivers
#include "src/drivers/pixy_spi.h"

// Debug
#include "src/debug/debug.h"


// SPI variables
SPI_Handle      spiHandle;
SPI_Params      spiParams;
SPI_Transaction spiTransaction;

/***************************************** SPI FUNCTIONS *****************************************/
/*
 * Setting up the SPI in continuous callback mode that calls the callback
 * function after every SPI transfer (recurrent 60 times a second).
 */
void spiInit(){
    SPI_Params_init(&spiParams);                        // Initialize SPI parameters
    spiParams.mode                  = SPI_MASTER;
    spiParams.bitRate               = 2000000;          // bit rate of pixy cam = 2MHz
    spiParams.dataSize              = 8;                // 8-bit data size
    spiParams.transferMode          = SPI_MODE_BLOCKING;//SPI_MODE_CALLBACK;
    spiParams.transferCallbackFxn   = spiCallbackFxn;


    spiHandle = SPI_open(CONFIG_SPI_MASTER, &spiParams);
    if (spiHandle == NULL) {
        //dbgOutputLoc(DLOC_SPI_OPEN_FAIL);
        systemExit();
    }

    const char msg1[] = "spi-init\n\r";
    dbgUARTMsg(msg1, sizeof(msg1));
}

void spiCallbackFxn(SPI_Handle spi, SPI_Transaction *tran){

    uint8_t * rx_Buff = tran -> rxBuf;
    //uint8_t type = (uint8_t) (tran -> arg);
    uint8_t size = sizeof(rx_Buff)/sizeof(rx_Buff[0]);

    pushToRxQueue(-1);
    pushToRxQueue(0);
    pushToRxQueue(size);

    int i;
    for(i = 0; i < 10; i++){
        pushToRxQueue((int16_t) rx_Buff[i]);
    }

}

void disableSPI(){
    SPI_close(spiHandle);
}
