/*
 * pixy_spi.h
 *
 *  Created on: Mar 3, 2020
 *      Author: singularity
 */

// Driver configuration
#include <ti/drivers/SPI.h>
#include "ti_drivers_config.h"

#ifndef SRC_DRIVERS_PIXY_SPI_H_
#define SRC_DRIVERS_PIXY_SPI_H_

// SPI functions prototypes
void spiInit();
void spiCallbackFxn();
void disableSPI();
void spiCallbackFxn(SPI_Handle spi, SPI_Transaction *tran);



#endif /* SRC_DRIVERS_PIXY_SPI_H_ */
