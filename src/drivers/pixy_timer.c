/*
 * pixy_timer.c
 *
 *  Created on: Mar 3, 2020
 *      Author: Sumved Ravi
 */

// Standard library imports
#include <stddef.h>
#include <string.h>

// Driver configuration
#include <ti/drivers/Board.h>
#include <ti/drivers/Timer.h>
#include <ti/drivers/UART.h>
#include <ti/drivers/GPIO.h>
#include <ti/drivers/SPI.h>
#include "ti_drivers_config.h"

// Custom queues
#include "src/queues/timer_queue.h"

// Custom drivers
#include "src/drivers/pixy_timer.h"

// Debug
#include "src/debug/debug.h"

// Timer variables
Timer_Handle    timerHandle;
Timer_Params    timerParams;

/***************************************** TIMER FUNCTIONS ***************************************/
/*
 * Setting up the timer in continuous callback mode that calls the callback
 * function every 10 microseconds.
 */
void timerInit(){
    // Timer One Parameters and Initialization
    Timer_Params_init(&timerParams);
    timerParams.period = TIMER_PERIOD;
    timerParams.periodUnits = Timer_PERIOD_US;
    timerParams.timerMode = Timer_CONTINUOUS_CALLBACK;
    timerParams.timerCallback = timerCallbackFxn;

    timerHandle = Timer_open(CONFIG_TIMER_0, &timerParams);

    if (timerHandle == NULL) {
        // Failed to initialized timer
        //dbgOutputLoc(DLOC_TIMER_OPEN_FAIL);
        systemExit();
    }

    if (Timer_start(timerHandle) == Timer_STATUS_ERROR ) {
        // Failed to start timer
        //dbgOutputLoc(DLOC_TIMER_START_FAIL);
        systemExit();
    }

    const char msg1[] = "timer-init\n\r";
    dbgUARTMsg(msg1, sizeof(msg1));
}

void timerCallbackFxn(Timer_Handle myHandle){
    static uint32_t timer_count = 0;

    if (timer_count % PIXY_PERIOD == 0){ //TIMER_COUNT_HIT
        GPIO_toggle(CONFIG_GPIO_LED_0);
        pushToTimerQueue(timer_count);
    }

    if(timer_count >= TIMER_MAX){
        timer_count = 0;
    }

    timer_count++;
}

/*
 * Disables timer, halting possibility of interrupts
 */
void disableTimer(){
    Timer_stop(timerHandle);
    Timer_close(timerHandle);
}

