/*
 * pixy_timer.h
 *
 *  Created on: Mar 3, 2020
 *      Author: Sumved Ravi
 */

// Driver configuration
#include <ti/drivers/Timer.h>
#include "ti_drivers_config.h"

#ifndef SRC_DRIVERS_PIXY_TIMER_H_
#define SRC_DRIVERS_PIXY_TIMER_H_

// Constant variables
#define TIMER_PERIOD    (10)            // 10 microseconds
#define PIXY_PERIOD     (200000)        //5secs rn =500000 // 1667 for 60 hz //100000? for 1 second//4294967295 = max u32 bit value
#define TIMER_MAX       (1000000000)    //4294967295 = max u32 bit value

// Timer functions prototypes
void timerInit();
void timerCallbackFxn(Timer_Handle myHandle);
void disableTimer();

#endif /* SRC_DRIVERS_PIXY_TIMER_H_ */
