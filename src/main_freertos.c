/*
 * Copyright (c) 2016-2019, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== main_freertos.c ========
 */
#include <stdint.h>

#ifdef __ICCARM__
#include <DLib_Threads.h>
#endif

/* POSIX Header files */
#include <pthread.h>

/* RTOS header files */
#include <FreeRTOS.h>
#include <task.h>

// Driver configuration
#include <ti/drivers/Board.h>
#include <ti/drivers/Timer.h>
#include <ti/drivers/UART.h>
#include <ti/drivers/GPIO.h>
#include <ti/drivers/SPI.h>
#include "ti_drivers_config.h"

// Custom drivers
#include "src/drivers/pixy_timer.h"
#include "src/drivers/pixy_spi.h"

// Custom threads
#include "src/tasks/communication_task.h"
#include "src/tasks/summary_task.h"

// Validation
#include "src/validate/validation_task.h"

// Custom queues
#include "src/queues/command_queue.h"
#include "src/queues/timer_queue.h"
#include "src/queues/tx_queue.h"
#include "src/queues/rx_queue.h"

// Debug
#include "src/debug/debug.h"
//#include src/queues/debug_queue.h

// Validation
//#include "validate/validation_task.h"

// Prototypes
void launchThread(void* (threadMain)(void*), int priority, int stackSize, int debugID);

/* Stack size in bytes */
#define PIXY_COMMUNICATION_STACK_SIZE   1024


/*
 *  ======== main ========
 */
int main(void)
{

    /* initialize the system locks */
#ifdef __ICCARM__
    __iar_Initlocks();
#endif

    // Primary initialization
    Board_init();

    // Queue initializations (must take place prior to any driver inits)
    initPixyCommandQueue();
    initTimerQueue();
    initTxQueue();
    initRxQueue();

    // Driver initializations (preparing hardware)
    GPIO_init();
    Timer_init();
    UART_init();
    SPI_init();

    // Debug initializations (requires UART and GIPIO initializations first)
    // must take place prior to spinning up threads
    initDebug();
    //dbgOutputLoc(DLOC_MAIN_DEBUG_INIT);


    // Thread initializations - pixy thread
    launchThread(pixyCommunicationThread, 1, PIXY_COMMUNICATION_STACK_SIZE, DLOC_PIXY_COMM_TASK_FAIL);
    launchThread(pixyValidationThread, 1, PIXY_COMMUNICATION_STACK_SIZE, DLOC_PIXY_COMM_TASK_FAIL);
    launchThread(pixySummaryThread, 1, PIXY_COMMUNICATION_STACK_SIZE, DLOC_PIXY_COMM_TASK_FAIL);

    /* Start the FreeRTOS scheduler */
    vTaskStartScheduler();

    return (0);
}


/*
 * Launch a detached thread to be scheduled by FreeRTOS
 */
void launchThread(void* (threadMain)(void*), int priority, int stackSize, int debugID)
{
     pthread_t           thread;
     pthread_attr_t      attrs;
     struct sched_param  priParam;
     int                 retc;

     // Initialize the attributes structure with default values
     pthread_attr_init(&attrs);

     // Set priority, detach state, and stack size attributes
     priParam.sched_priority = priority;
     retc = pthread_attr_setschedparam(&attrs, &priParam);
     retc |= pthread_attr_setdetachstate(&attrs, PTHREAD_CREATE_DETACHED);
     retc |= pthread_attr_setstacksize(&attrs, stackSize);
     if (retc != 0)
     {
         // failed to set attributes
         dbgOutputLoc(debugID);
         systemExit();
     }

     retc = pthread_create(&thread, &attrs, threadMain, NULL);
     if (retc != 0)
     {
         // pthread_create() failed
         dbgOutputLoc(debugID);
         systemExit();
     }

}





//*****************************************************************************
//
//! \brief Application defined malloc failed hook
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void vApplicationMallocFailedHook()
{
    /* Handle Memory Allocation Errors */
    while(1)
    {
    }
}

//*****************************************************************************
//
//! \brief Application defined stack overflow hook
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void vApplicationStackOverflowHook(TaskHandle_t pxTask, char *pcTaskName)
{
    //Handle FreeRTOS Stack Overflow
    while(1)
    {
    }
}
