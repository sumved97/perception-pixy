/*
 * command_queue.c
 *
 *  Created on: Mar 3, 2020
 *      Author: Sumved Ravi
 */

// FreeRTOS includes
#include <FreeRTOS.h>
#include <queue.h>

// Module includes
#include "src/queues/command_queue.h"
#include "src/debug/debug.h"

static QueueHandle_t xPixyCommandQueue;

void initPixyCommandQueue(){
    xPixyCommandQueue = xQueueCreate(100, sizeof(uint32_t));

    if(xPixyCommandQueue == NULL){
        //dbgOutputLoc(DLOC_TIMER_QUEUE_INIT_FAIL);
        systemExit();
    }
}
void pushToPixyCommandQueue(uint8_t timerCount){
    BaseType_t retCode = xQueueSendFromISR(xPixyCommandQueue, &timerCount, NULL);

    if(retCode == errQUEUE_FULL){
        //dbgOutputLoc(DLOC_TIMER_QUEUE_PUSH_FAIL);
        systemExit();
    }
}
void popFromPixyCommandQueue(uint8_t* timerCount){
    BaseType_t retCode = xQueueReceive(xPixyCommandQueue, timerCount, portMAX_DELAY);

    if(retCode == pdFALSE){
        //dbgOutputLoc(DLOC_TIMER_QUEUE_POP_FAIL);
        systemExit();
    }
}


