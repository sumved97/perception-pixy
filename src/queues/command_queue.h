/*
 * command_queue.h
 *
 *  Created on: Mar 3, 2020
 *      Author: Sumved Ravi
 */

#ifndef SRC_QUEUES_COMMAND_QUEUE_H_
#define SRC_QUEUES_COMMAND_QUEUE_H_

void initPixyCommandQueue();
void pushToPixyCommandQueue(uint8_t timerCount);
void popFromPixyCommandQueue(uint8_t* timerCount);

#endif /* SRC_QUEUES_COMMAND_QUEUE_H_ */
