/*
 * rx_queue.c
 *
 *  Created on: Mar 3, 2020
 *      Author: Sumved Ravi
 */
// FreeRTOS includes
#include <FreeRTOS.h>
#include <queue.h>

// Custom queues
#include "src/queues/rx_queue.h"

// Debug
#include "src/debug/debug.h"

static QueueHandle_t xRxQueue;

void initRxQueue(){
    xRxQueue = xQueueCreate(100, sizeof(int16_t));

    if(xRxQueue == NULL){
        //dbgOutputLoc(DLOC_TIMER_QUEUE_INIT_FAIL);
        systemExit();
    }
}

void pushToRxQueue(int16_t dataByte){
    BaseType_t retCode = xQueueSendFromISR(xRxQueue, &dataByte, NULL);

    if(retCode == errQUEUE_FULL){
        //dbgOutputLoc(DLOC_TIMER_QUEUE_PUSH_FAIL);
        systemExit();
    }
}

void popFromRxQueue(int16_t* dataByte){
    BaseType_t retCode = xQueueReceive(xRxQueue, dataByte, portMAX_DELAY);

    if(retCode == pdFALSE){
        //dbgOutputLoc(DLOC_TIMER_QUEUE_POP_FAIL);
        systemExit();
    }
}
