/*
 * rx_queue.h
 *
 *  Created on: Mar 3, 2020
 *      Author: singularity
 */

#ifndef SRC_QUEUES_RX_QUEUE_H_
#define SRC_QUEUES_RX_QUEUE_H_

void initRxQueue();
void pushToRxQueue(int16_t dataByte);
void popFromRxQueue(int16_t* dataByte);

#endif /* SRC_QUEUES_RX_QUEUE_H_ */
