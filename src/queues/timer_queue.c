/*
 * timer_queue.c
 *
 *  Created on: Mar 3, 2020
 *      Author: Sumved Ravi
 */
// FreeRTOS includes
#include <FreeRTOS.h>
#include <queue.h>

// Custom queues
#include "src/queues/timer_queue.h"

// Debug
#include "src/debug/debug.h"

static QueueHandle_t xTimerQueue;

void initTimerQueue(){
    xTimerQueue = xQueueCreate(100, sizeof(uint32_t));

    if(xTimerQueue == NULL){
        //dbgOutputLoc(DLOC_TIMER_QUEUE_INIT_FAIL);
        systemExit();
    }
}

void pushToTimerQueue(uint32_t timerCount){
    BaseType_t retCode = xQueueSendFromISR(xTimerQueue, &timerCount, NULL); //NULL -> portMAX_DELAY

    if(retCode == errQUEUE_FULL){
        //dbgOutputLoc(DLOC_TIMER_QUEUE_PUSH_FAIL);
        systemExit();
    }
}

void popFromTimerQueue(uint32_t* timerCount){
    BaseType_t retCode = xQueueReceive(xTimerQueue, timerCount, portMAX_DELAY);

    if(retCode == pdFALSE){
        //dbgOutputLoc(DLOC_TIMER_QUEUE_POP_FAIL);
        systemExit();
    }
}
