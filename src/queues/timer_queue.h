/*
 * timer_queue.h
 *
 *  Created on: Mar 3, 2020
 *      Author: singularity
 */

#ifndef SRC_QUEUES_TIMER_QUEUE_H_
#define SRC_QUEUES_TIMER_QUEUE_H_

void initTimerQueue();
void pushToTimerQueue(uint32_t timerCount);
void popFromTimerQueue(uint32_t* timerCount);

#endif /* SRC_QUEUES_TIMER_QUEUE_H_ */
