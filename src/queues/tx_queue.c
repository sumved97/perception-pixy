/*
 * tx_queue.c
 *
 *  Created on: Mar 3, 2020
 *      Author: Sumved Ravi
 */

// FreeRTOS includes
#include <FreeRTOS.h>
#include <queue.h>

// Custom queues
#include "src/queues/tx_queue.h"

// Debug
#include "src/debug/debug.h"

static QueueHandle_t xTxQueue;

void initTxQueue(){
    xTxQueue = xQueueCreate(100, sizeof(int16_t));

    if(xTxQueue == NULL){
        //dbgOutputLoc(DLOC_TIMER_QUEUE_INIT_FAIL);
        systemExit();
    }
}

void pushToTxQueue(int16_t dataByte){
    BaseType_t retCode = xQueueSendFromISR(xTxQueue, &dataByte, NULL);

    if(retCode == errQUEUE_FULL){
        //dbgOutputLoc(DLOC_TIMER_QUEUE_PUSH_FAIL);
        systemExit();
    }
}

void popFromTxQueue(int16_t* dataByte){
    BaseType_t retCode = xQueueReceive(xTxQueue, dataByte, portMAX_DELAY);

    if(retCode == pdFALSE){
        //dbgOutputLoc(DLOC_TIMER_QUEUE_POP_FAIL);
        systemExit();
    }
}
