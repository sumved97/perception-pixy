/*
 * tx_queue.h
 *
 *  Created on: Mar 3, 2020
 *      Author: singularity
 */

#ifndef SRC_QUEUES_TX_QUEUE_H_
#define SRC_QUEUES_TX_QUEUE_H_

void initTxQueue();
void pushToTxQueue(int16_t dataByte);
void popFromTxQueue(int16_t* dataByte);

#endif /* SRC_QUEUES_TX_QUEUE_H_ */
