/*
 * communication_task.c
 *
 *  Created on: Mar 3, 2020
 *      Author: Sumved Ravi
 */

// Standard library imports
#include <stddef.h>
#include <string.h>

// Driver configuration
#include <ti/drivers/Board.h>
#include <ti/drivers/Timer.h>
#include <ti/drivers/UART.h>
#include <ti/drivers/GPIO.h>
#include <ti/drivers/SPI.h>
#include "ti_drivers_config.h"

// Custom threads
#include "src/tasks/communication_task.h"

// Custom queues
#include "src/queues/command_queue.h"
#include "src/queues/timer_queue.h"
#include "src/queues/tx_queue.h"
#include "src/queues/rx_queue.h"

// Custom drivers
#include "src/drivers/pixy_timer.h"
#include "src/drivers/pixy_spi.h"

// Debug
#include "src/debug/debug.h"

// Private functions
void pixy_setLED();
void pixy_getRGB();

// Variables
enum pixy_msg {SET_LED, GET_RGB};

extern SPI_Handle spiHandle;
extern SPI_Params spiParams;
extern SPI_Transaction spiTransaction;

extern dataType;
/***************************************** THREAD FUNCTIONS **************************************/
void *pixyCommunicationThread(void *arg0){
    // Initialize Hardware Settings
    spiInit();
    timerInit();

    while(1){
        uint8_t command;

        popFromPixyCommandQueue(&command);
        switch(command){
        case SET_LED:   pixy_setLED();
                        break;
        case GET_RGB:   pixy_getRGB();
                        break;
        default:        //DBG HERE
                        systemExit();
                        break;
        }
    }
}

void pixy_setLED(){
    uint8_t type = SET_LED;
    static uint8_t inc = 0;
    bool transferOK;

    uint8_t tx_Buff[10] = {174, 193, 20, 3, 255, 0, 0, 0, 0}; //needs only first 7 values
    uint8_t rx_Buff[10];
    uint8_t size = (int16_t)sizeof(rx_Buff);

    if(inc%2 == 0){
        tx_Buff[4] = 0;
        tx_Buff[5] = 255;
    }

    if(inc == 255)
        inc = 0;
    inc++;

    //spiTransaction.arg = &type;
    spiTransaction.count = 10;
    spiTransaction.txBuf = (void *) tx_Buff;
    spiTransaction.rxBuf = (void *) rx_Buff;

    //pushToRxQueue(-1);
    //pushToRxQueue(type);
    //pushToRxQueue(size);

    transferOK = SPI_transfer(spiHandle, &spiTransaction);
    if(!transferOK){
        //PRINT DBG OUT VALUE
        systemExit();
    }

    int i;
    for(i = 0; i < size; i++){
        pushToRxQueue((int16_t) rx_Buff[i]);
    }
}

void pixy_getRGB(){
    bool transferOK;
    uint8_t type = GET_RGB;


    uint8_t tx_Buff[9] = {174, 193, 112, 5, 0, 150, 0, 100, 0}; //needs only first 9 values
    uint8_t rx_Buff[9];
    uint8_t size = (int16_t)sizeof(rx_Buff);

    spiTransaction.arg = &type;
    spiTransaction.count = 9;
    spiTransaction.txBuf = (void *) tx_Buff;
    spiTransaction.rxBuf = (void *) rx_Buff;


    transferOK = SPI_transfer(spiHandle, &spiTransaction);
    if(!transferOK){
        //PRINT DBG OUT VALUE
        systemExit();
    }

    //pushToRxQueue(-1);
    //pushToRxQueue(type);
    //pushToRxQueue( (int16_t)sizeof(rx_Buff) );

    int i;
    for(i = 0; i < size; i++){
        pushToRxQueue((int16_t) rx_Buff[i]);
    }
}
