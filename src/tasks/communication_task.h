/*
 * communication_task.h
 *
 *  Created on: Mar 3, 2020
 *      Author: Sumved Ravi
 */

#ifndef SRC_TASKS_COMMUNICATION_TASK_H_
#define SRC_TASKS_COMMUNICATION_TASK_H_

void* pixyCommunicationThread(void* arg0);

#endif /* SRC_TASKS_COMMUNICATION_TASK_H_ */
