/*
 * summary_task.c
 *
 *  Created on: Mar 3, 2020
 *      Author: Sumved Ravi
 */

// Standard library imports
#include <stddef.h>
#include <string.h>

// Driver configuration
#include <ti/drivers/Board.h>
#include "ti_drivers_config.h"

// Custom threads
#include "src/tasks/summary_task.h"

// Custom queues
#include "src/queues/command_queue.h"
#include "src/queues/timer_queue.h"
#include "src/queues/tx_queue.h"
#include "src/queues/rx_queue.h"

// Debug
#include "src/debug/debug.h"

void *pixySummaryThread(void *arg0){

    const char msg1[] = "summary-init\n\r";
    dbgUARTMsg(msg1, sizeof(msg1));

    enum dataType {START, TYPE, SIZE, PACKET, SYNC_1, SYNC_2};
    uint8_t receivePacket[100];
    int16_t payload_size = 0;
    //uint8_t type = 0;
    int16_t dataByte;
    uint8_t curRead = SYNC_1;

    while(1){
        popFromRxQueue(&dataByte);
        dbgUARTInt(dataByte);
        dbgUARTMsg(", ", 2);

        if(dataByte == 175 && curRead == SYNC_1){
            dbgUARTMsg("\n\r", 2);
            dbgUARTMsg("Sync: ", 6);
            curRead = SYNC_2;
            dbgUARTInt(dataByte);
            dbgUARTMsg(", ", 2);
        }
        if(dataByte == 193 && curRead == SYNC_2){
            curRead = PACKET;
            dbgUARTInt(dataByte);
            dbgUARTMsg("\n\r", 2);
        }
        else if(dataByte != 193 && curRead == SYNC_2){
            curRead = SYNC_1;
        }

        if(curRead == PACKET){
            dbgUARTMsg("Packet: ", 8);
            dbgUARTInt(dataByte);

            popFromRxQueue(&dataByte);
            if(dataByte != 1)
                break;

            dbgUARTInt(dataByte);
            dbgUARTMsg(", ", 2);

            popFromRxQueue(&dataByte);
            payload_size = (int16_t) dataByte;
            dbgUARTInt(payload_size);
            dbgUARTMsg(", ", 2);

            popFromRxQueue(&dataByte);
            int8_t checksum_1 = dataByte;
            dbgUARTInt(checksum_1);
            dbgUARTMsg(", ", 2);

            popFromRxQueue(&dataByte);
            int8_t checksum_2 = dataByte;
            dbgUARTInt(checksum_2);
            dbgUARTMsg(", ", 2);

            uint8_t i;
            for(i = 0; i < payload_size; i++){
                popFromRxQueue(&dataByte);

                receivePacket[i] = dataByte;
                dbgUARTInt(receivePacket[i]);
                dbgUARTMsg(", ", 2);
            }

            /*
            if(receivePacket[0] > 0 && payload_size ==4){
                pixy_getRGBSummary(receivePacket);
            }
            else{
                pixy_setLEDSummary();
            }
            */

            curRead = SYNC_1;
        }
    }
}
void pixy_setLEDSummary(){
    dbgUARTMsg("\n\r", 2);
    dbgUARTMsg("\n\r", 2);
    dbgUARTMsg("\n\r", 2);
}
void pixy_getRGBSummary(uint8_t receivePacket[]){

    dbgUARTMsg("R: ", 2);
    dbgUARTInt(receivePacket[0]);
    dbgUARTMsg("G: ", 2);
    dbgUARTInt(receivePacket[0]);
    dbgUARTMsg("B: ", 2);
    dbgUARTInt(receivePacket[0]);

    dbgUARTMsg("\n\r", 2);
    dbgUARTMsg("\n\r", 2);
    dbgUARTMsg("\n\r", 2);
}
