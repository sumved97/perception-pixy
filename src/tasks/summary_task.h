/*
 * summary_task.h
 *
 *  Created on: Mar 3, 2020
 *      Author: Sumved Ravi
 */

#ifndef SRC_TASKS_SUMMARY_TASK_H_
#define SRC_TASKS_SUMMARY_TASK_H_

void* pixySummaryThread(void* arg0);
void pixy_setLEDSummary();
void pixy_getRGBSummary(uint8_t receivePacket[]);

#endif /* SRC_TASKS_SUMMARY_TASK_H_ */
