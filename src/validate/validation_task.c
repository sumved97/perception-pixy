/*
 * validation_task.c
 *
 *  Created on: Mar 3, 2020
 *      Author: Sumved Ravi
 */

// Standard library imports
#include <stddef.h>
#include <string.h>

// Driver configuration
#include <ti/drivers/Board.h>
#include <ti/drivers/GPIO.h>
#include "ti_drivers_config.h"

// Custom drivers
#include "src/drivers/pixy_timer.h"

// Validation
#include "src/validate/validation_task.h"

// Custom queues
#include "src/queues/command_queue.h"
#include "src/queues/timer_queue.h"

// Debug
#include "src/debug/debug.h"

/**************************************************************************************************/

void *pixyValidationThread(void *arg0){
    uint32_t timerCount;
    uint8_t state;

    while(1){
        popFromTimerQueue(&timerCount);

        if(timerCount%PIXY_PERIOD == 0){
            state = GPIO_read(CONFIG_GPIO_VALIDATION);
            pushToPixyCommandQueue(state);
        }
    }
}


