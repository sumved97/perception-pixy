/*
 * validation_task.h
 *
 *  Created on: Mar 3, 2020
 *      Author: Sumved Ravi
 */

#ifndef SRC_VALIDATE_VALIDATION_TASK_H_
#define SRC_VALIDATE_VALIDATION_TASK_H_

void* pixyValidationThread(void* arg0);

#endif /* SRC_VALIDATE_VALIDATION_TASK_H_ */
